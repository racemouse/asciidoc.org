export function init () {
  find('.toggleset').forEach(function (toggleset) {
    find(':scope > .toggle > a', toggleset).forEach(function (item) {
      item.addEventListener('click', function (event) {
        find(':scope > .toggle > a', toggleset).forEach(function (it) {
          it === item ? it.classList.add('is-active') : it.classList.remove('is-active')
        })
        find(':scope > .content > *', toggleset).forEach(function (it) {
          it.getAttribute('aria-labelledby') === item.id ? it.classList.add('is-active') : it.classList.remove('is-active')
        })
      })
    })
  })

  function find (selector, from) {
    return Array.prototype.slice.call((from || document).querySelectorAll(selector))
  }
}
